import Vue from "vue";
import Router from "vue-router";
import baseCount from "@/components/baseCount";

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: "/",
      name: "baseCount",
      component: baseCount
    }
  ]
});
